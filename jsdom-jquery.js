"use strict";

let URL = require('url');
let https = require('https');
let jsdom = require('jsdom').jsdom;
let jquery = require('jquery');

module.exports = (url, options = {}) => {
    let { protocol, hostname, port, path } = URL.parse(url);

    options = {...options, protocol, hostname, port, path };

    return new Promise((resolve, reject) => {
        https.get(options, res => {
            let content = "";
            res.on('data', chunk => content += chunk);
            res.on('end', () => {
                let window = jsdom(content).defaultView;
                let $ = jquery(window);
                resolve({$, window});
            });
        }).on('error', reject);
    });
};
