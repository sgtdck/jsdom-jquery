# jsdom-jquery

## Usage

```javascript
"use strict";
let jj = require('jsdom-jquery');
jj("https://www.npmjs.com/").then(({ $, window }) => {
    console.log("Title: " + $('title').text());
    console.log("Description: " + $('meta[name=description]').attr('content'));
    window.close();
}).catch(e => console.log(e.message));
```

You must call `window.close()` for the connection to end properly.
